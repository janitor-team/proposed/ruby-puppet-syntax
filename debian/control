Source: ruby-puppet-syntax
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Gaudenz Steinlin <gaudenz@debian.org>,
           Georg Faerber <georg@debian.org>,
           Sebastien Badia <sbadia@debian.org>
Build-Depends: debhelper-compat (= 13),
               gem2deb (>= 1),
               puppet,
               rake,
               ruby-rspec,
               ruby-sync
Standards-Version: 4.6.1
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-puppet-syntax.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-puppet-syntax
Homepage: https://github.com/voxpupuli/puppet-syntax
Testsuite: autopkgtest-pkg-ruby
XS-Ruby-Versions: all
Rules-Requires-Root: no

Package: ruby-puppet-syntax
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: puppet,
         rake,
         ${misc:Depends},
         ${ruby:Depends},
         ${shlibs:Depends}
Description: Syntax checks for Puppet manifests, templates, and Hiera YAML
 Puppet lets you centrally manage every important aspect of your
 system using a cross-platform specification language that manages all
 the separate elements normally aggregated in different files, like
 users, cron jobs, and hosts, along with obviously discrete elements
 like packages, services, and files.
 .
 This ruby module provides syntax checks for Puppet manifests,
 templates and Hiera YAML.
